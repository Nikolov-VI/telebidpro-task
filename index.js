const N = 20;
const K = 3;
const goats = [52, 17946, 27160, 387, 17346, 27505, 20816, 20577, 10961, 6021, 5262, 28278, 24163, 931, 11003, 19738, 17914, 1683, 10320, 10475];

function canRaftCarryGoats(raftCapacity, goats, maxCourses) {
    const goatsCopy = [...goats];

    for(let i = 1; i <= maxCourses; i++) {
        let capacityCopy = raftCapacity;

        for(let j = goatsCopy.length - 1; j >= 0; j--) {
            if(capacityCopy - goatsCopy[j] >= 0) {
                capacityCopy -= goatsCopy[j];
                goatsCopy.splice(j, 1);
                
            }
            
            if(capacityCopy <= 0) break;
        }
    }

    return goatsCopy.length === 0;
}

function binarySearchForCapacity(goats, maxCourses, minCapacity, maxCapacity) {
    if (minCapacity > maxCapacity) return minCapacity;
 
    let midCapacity = Math.floor((minCapacity + maxCapacity) / 2);

    if (canRaftCarryGoats(midCapacity, goats, maxCourses)) {
        return binarySearchForCapacity(goats, maxCourses, minCapacity, midCapacity - 1);
    } else {
        return binarySearchForCapacity(goats, maxCourses, midCapacity + 1, maxCapacity)
    }
}

function raft(goatAmount, maxCourses, goatArr) {
    goatArr.sort((a, b) => a - b);
    const minCapacity = goatArr[goatArr.length - 1];
    const maxCapacity = 1000 * 100000;
    
    const raftCapacity = binarySearchForCapacity(goats, maxCourses, minCapacity, maxCapacity);
    console.log(raftCapacity);
    return raftCapacity;
}

raft(N, K, goats);